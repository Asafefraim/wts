using FluentAssertions;
using System;
using System.Collections.Generic;
using WTS.Logic.LogicModels;
using Xunit;

namespace WTS.Logic.Tests
{
    public class ShiftsAggregatorTests
    {
        public DateTime EndTime { get; private set; }

        [Fact]
        public void AggregateShiftsHours_WithEmptyList_ReturnsZero()
        {
            var aggregator = new ShiftsAggregator();
            var now = DateTime.UtcNow;
            var result = aggregator.AggregateShiftsHours(
                new List<Shift>(),
                now,
                now.AddDays(1));

            result.Should().Be(0);
        }

        [Fact]
        public void AggregateShiftsHours_WithSingleItemInList_ReturnsCorrectNumber()
        {
            var aggregator = new ShiftsAggregator();
            var now = DateTime.UtcNow;
            var result = aggregator.AggregateShiftsHours(new List<Shift>
            {
                new Shift{StartTime = now, EndTime = now.AddHours(1.5)}
            }, now, now.AddDays(1));

            result.Should().Be(1.5);
        }

        [Fact]
        public void AggregateShiftsHours_WithMultipleItemsInList_ReturnsCorrectNumber()
        {
            var aggregator = new ShiftsAggregator();
            var now = DateTime.UtcNow;
            var result = aggregator.AggregateShiftsHours(new List<Shift>
            {
                new Shift{StartTime = now, EndTime = now.AddHours(1.5)},
                new Shift{StartTime = now.AddDays(1), EndTime = now.AddDays(1).AddHours(3)}
            }, now, now.AddDays(3));

            result.Should().Be(4.5);
        }

        [Fact]
        public void AggregateShiftsHours_WhenListStartsBeforeTimeWindow_OnlyConsidersThePartOfTheShiftWithinTheTimeWindow()
        {
            var aggregator = new ShiftsAggregator();
            var now = DateTime.UtcNow;
            var result = aggregator.AggregateShiftsHours(new List<Shift>
            {
                new Shift{StartTime = now.AddHours(-1), EndTime = now.AddHours(5)},
            }, now, now.AddDays(1));

            result.Should().Be(5);
        }

        [Fact]
        public void AggregateShiftsHours_WhenListEndsAfterTimeWindow_OnlyConsidersThePartOfTheShiftWithinTheTimeWindow()
        {
            var aggregator = new ShiftsAggregator();
            var now = DateTime.UtcNow;
            var result = aggregator.AggregateShiftsHours(new List<Shift>
            {
                new Shift{StartTime = now, EndTime = now.AddHours(5)},
            }, now, now.AddHours(1.5));

            result.Should().Be(1.5);
        }
    }
}
