using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using System;
using System.Collections.Generic;
using WTS.Controllers;
using WTS.Data;
using WTS.Logic;
using WTS.Models;
using Xunit;

namespace WTS.Tests
{
    public class HomeControllerTests
    {
        #region Index

        [Fact]
        public void Index_CallsRepositoryForListOfEmployees()
        {
            var repository = Substitute.For<IShiftsRepository>();
            var mapper = Substitute.For<IMapper>();
            var aggregator = Substitute.For<IShiftsAggregator>();

            var homeController = new HomeController(repository, mapper, aggregator);
            homeController.Index();

            repository.Received().ListEmployees();
        }

        [Fact]
        public void Index_MapsListOfEmployeesToApiModel()
        {
            var repository = Substitute.For<IShiftsRepository>();
            IEnumerable<Logic.LogicModels.Employee> employeeList = new List<Logic.LogicModels.Employee>
            {
                new Logic.LogicModels.Employee { EmployeeId = 6 }
            };

            repository.ListEmployees().Returns(employeeList);

            var mapper = Substitute.For<IMapper>();
            var aggregator = Substitute.For<IShiftsAggregator>();

            var homeController = new HomeController(repository, mapper, aggregator);
            homeController.Index();

            mapper
                .Received()
                .Map<IEnumerable<Models.Employee>>(employeeList);
        }

        [Fact]
        public void Index_ReturnedViewContainsExpectedListOfEmployees()
        {
            var repository = Substitute.For<IShiftsRepository>();
            IEnumerable<Logic.LogicModels.Employee> employeeList = new List<Logic.LogicModels.Employee>
            {
                new Logic.LogicModels.Employee { EmployeeId = 6 }
            };

            repository.ListEmployees().Returns(employeeList);

            var mapper = Substitute.For<IMapper>();
            var mappedList = new List<Models.Employee> { new Models.Employee { EmployeeId = 6 } };
            mapper
                .Map<IEnumerable<Models.Employee>>(employeeList)
                .Returns(mappedList);

            var aggregator = Substitute.For<IShiftsAggregator>();

            var homeController = new HomeController(repository, mapper, aggregator);
            var result = (ViewResult)homeController.Index();
            var model = (QueryMonthlyHours)result.Model;

            model.Employees.Should().BeEquivalentTo(mappedList);
        }

        #endregion

        #region GetMonthlyHours

        [Fact]
        public void GetMonthlyHours_WhenModelIsInvalid_ReturnsError()
        {
            var repository = Substitute.For<IShiftsRepository>();

            var employeeShifts = new Logic.LogicModels.EmployeeShifts { Shifts = new List<Logic.LogicModels.Shift>() };
            repository
                .GetEmployeeShifts(0, default(DateTime), default(DateTime))
                .ReturnsForAnyArgs(employeeShifts);

            var mapper = Substitute.For<IMapper>();
            var aggregator = Substitute.For<IShiftsAggregator>();

            var controller = new HomeController(repository, mapper, aggregator);
            controller.ModelState.AddModelError("error", "error message");

            var model = new QueryMonthlyHours { EmployeeId = 3, SelectedMonth = DateTime.UtcNow };
            var result = (RedirectToActionResult)controller.GetMonthlyHours(model);
            result.ActionName.Should().Be(nameof(HomeController.Error));
        }

        [Fact]
        public void GetMonthlyHours_CallsRepositoryWithEmployeeIdAndMonth()
        {
            var repository = Substitute.For<IShiftsRepository>();
            var mapper = Substitute.For<IMapper>();
            var aggregator = Substitute.For<IShiftsAggregator>();

            var employeeShifts = new Logic.LogicModels.EmployeeShifts { Shifts = new List<Logic.LogicModels.Shift>() };
            repository
                .GetEmployeeShifts(0, default(DateTime), default(DateTime))
                .ReturnsForAnyArgs(employeeShifts);

            var controller = new HomeController(repository, mapper, aggregator);

            var employeeId = 4;
            var arbitraryDayInMonth = DateTime.Parse("2018-04-17");
            var startOfMonth = DateTime.Parse("2018-04-01");
            var endOfMonth = DateTime.Parse("2018-05-01").AddTicks(-1);
            var model = new QueryMonthlyHours { EmployeeId = employeeId, SelectedMonth = arbitraryDayInMonth };
            controller.GetMonthlyHours(model);

            repository.Received().GetEmployeeShifts(employeeId, startOfMonth, endOfMonth);
        }

        [Fact]
        public void GetMonthlyHours_ComputesMonthlyHoursUsingAggregator()
        {
            var repository = Substitute.For<IShiftsRepository>();
            var mapper = Substitute.For<IMapper>();
            var aggregator = Substitute.For<IShiftsAggregator>();

            var employeeShifts = new Logic.LogicModels.EmployeeShifts { Shifts = new List<Logic.LogicModels.Shift>() };
            repository
                .GetEmployeeShifts(0, default(DateTime), default(DateTime))
                .ReturnsForAnyArgs(employeeShifts);

            var arbitraryDayInMonth = DateTime.Parse("2018-04-17");
            var startOfMonth = DateTime.Parse("2018-04-01");
            var endOfMonth = DateTime.Parse("2018-05-01").AddTicks(-1);
            var model = new QueryMonthlyHours { EmployeeId = 1, SelectedMonth = arbitraryDayInMonth };

            var controller = new HomeController(repository, mapper, aggregator);
            controller.GetMonthlyHours(model);

            aggregator.Received().AggregateShiftsHours(employeeShifts.Shifts, startOfMonth, endOfMonth);
        }

        [Fact]
        public void GetMonthlyHours_ReturnsViewContainingExpectedAggregationResult()
        {
            var repository = Substitute.For<IShiftsRepository>();
            var mapper = Substitute.For<IMapper>();
            var aggregator = Substitute.For<IShiftsAggregator>();

            var employeeShifts = new Logic.LogicModels.EmployeeShifts { Shifts = new List<Logic.LogicModels.Shift>() };
            repository
                .GetEmployeeShifts(0, default(DateTime), default(DateTime))
                .ReturnsForAnyArgs(employeeShifts);

            int numberOfHours = 37;
            aggregator
                .AggregateShiftsHours(null, default(DateTime), default(DateTime))
                .ReturnsForAnyArgs(numberOfHours);

            var arbitraryDayInMonth = DateTime.Parse("2018-04-17");
            var startOfMonth = DateTime.Parse("2018-04-01");
            var endOfMonth = DateTime.Parse("2018-05-01").AddTicks(-1);
            var model = new QueryMonthlyHours { EmployeeId = 1, SelectedMonth = arbitraryDayInMonth };

            var controller = new HomeController(repository, mapper, aggregator);
            var view = (PartialViewResult)controller.GetMonthlyHours(model);
            var resultModel = (MonthlyHoursResults)view.Model;

            resultModel.ScheduledHours.Should().Be(numberOfHours);
        }

        #endregion
    }
}
