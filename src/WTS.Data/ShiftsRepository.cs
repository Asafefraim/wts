﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using WTS.Data;

namespace WTS
{
    public class ShiftsRepository : IShiftsRepository
    {
        private IMapper _mapper;
        private DataContext _dataContext;

        public ShiftsRepository(DataContext dataContext, IMapper mapper)
        {
            _mapper = mapper;
            _dataContext = dataContext;
        }

        public IEnumerable<Logic.LogicModels.Employee> ListEmployees()
        {
            var employees = _dataContext.Employees;
            return employees.Select(_mapper.Map<Logic.LogicModels.Employee>).ToList();
        }

        public Logic.LogicModels.EmployeeShifts GetEmployeeShifts(int employeeId, DateTime startTime, DateTime endTime)
        {
            var employee = _dataContext.Employees.FirstOrDefault(e => e.EmployeeId == employeeId);
            if (employee == null)
            {
                throw new EmployeeNotFoundException(employeeId);
            }

            var shifts = _dataContext.EmployeeShifts.Include(nameof(Data.DataModel.EmployeeShift.Shift))
                .Where(es => es.Employee.EmployeeId == employeeId)
                .Where(es =>
                    (es.Shift.StartTime >= startTime && es.Shift.StartTime <= endTime) ||
                    (es.Shift.EndTime >= startTime && es.Shift.EndTime <= endTime)).ToList();

            var mappedEmployee = _mapper.Map<Logic.LogicModels.Employee>(employee);
            var mappedShifts = shifts.Select(s=>s.Shift).Select(_mapper.Map<Logic.LogicModels.Shift>).ToList();

            return new Logic.LogicModels.EmployeeShifts { Employee = mappedEmployee, Shifts = mappedShifts };
        }
    }
}