﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WTS.Data.DataModel
{
    public class Shift
    {
        [Key]
        public int ShiftId { get; set; }

        public string ShiftName { get; set; }

        public DateTime StartTime { get;  set; }
        public DateTime EndTime { get; set; }
    }
}