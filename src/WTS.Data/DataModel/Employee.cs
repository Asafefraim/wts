﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WTS.Data.DataModel
{
    public class Employee
    {
        [Key]
        public int EmployeeId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}