﻿using System;
using System.Collections.Generic;

namespace WTS.Data
{
    public interface IShiftsRepository
    {
        IEnumerable<Logic.LogicModels.Employee> ListEmployees();
        Logic.LogicModels.EmployeeShifts GetEmployeeShifts(int employeeId, DateTime startTime, DateTime endTime);
    }
}