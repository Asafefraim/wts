﻿using Microsoft.EntityFrameworkCore;
using System;

namespace WTS
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Data.DataModel.EmployeeShift>().HasKey(
                    nameof(Data.DataModel.EmployeeShift.EmployeeId),
                    nameof(Data.DataModel.EmployeeShift.ShiftId));
        }   

        public DbSet<Data.DataModel.Employee> Employees { get; set; }
        public DbSet<Data.DataModel.Shift> Shifts { get; set; }
        public DbSet<Data.DataModel.EmployeeShift> EmployeeShifts { get; set; }
    }
}