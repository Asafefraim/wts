﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WTS.Data
{
    public class DataSeeder
    {
        public static void Seed(DataContext context)
        {
            context.Employees.AddRange(
                new DataModel.Employee { EmployeeId = 1, FirstName = "John", LastName = "Brown" },
                new DataModel.Employee { EmployeeId = 2, FirstName = "Harry", LastName = "Potter" },
                new DataModel.Employee { EmployeeId = 3, FirstName = "Alice", LastName = "White" },
                new DataModel.Employee { EmployeeId = 4, FirstName = "Joe", LastName = "Mellor" },
                new DataModel.Employee { EmployeeId = 5, FirstName = "Neil", LastName = "Fearn" },
                new DataModel.Employee { EmployeeId = 6, FirstName = "Rob", LastName = "Lang" });

            context.Shifts.AddRange(
                new DataModel.Shift { ShiftId = 1, ShiftName = "Morning 9-17", StartTime = DateTime.Parse("11 Nov 2016 09:00:00"), EndTime = DateTime.Parse("11 Nov 2016 17:00:00") },
                new DataModel.Shift { ShiftId = 2, ShiftName = "Morning 10-14", StartTime = DateTime.Parse("12 Nov 2016 10:00:00"), EndTime = DateTime.Parse("12 Nov 2016 14:00:00") },
                new DataModel.Shift { ShiftId = 4, ShiftName = "Morning 9-17", StartTime = DateTime.Parse("13 Nov 2016 09:00:00"), EndTime = DateTime.Parse("13 Nov 2016 17:00:00") },
                new DataModel.Shift { ShiftId = 5, ShiftName = "Morning 10-14", StartTime = DateTime.Parse("14 Nov 2016 10:00:00"), EndTime = DateTime.Parse("14 Nov 2016 14:00:00") },
                new DataModel.Shift { ShiftId = 6, ShiftName = "Morning 9-17", StartTime = DateTime.Parse("15 Nov 2016 09:00:00"), EndTime = DateTime.Parse("15 Nov 2016 17:00:00") },
                new DataModel.Shift { ShiftId = 7, ShiftName = "Morning 9-17", StartTime = DateTime.Parse("14 Dec 2016 09:00:00"), EndTime = DateTime.Parse("14 Dec 2016 17:00:00") },
                new DataModel.Shift { ShiftId = 8, ShiftName = "Morning 9-17", StartTime = DateTime.Parse("11 Nov 2016 09:00:00"), EndTime = DateTime.Parse("11 Nov 2016 17:00:00") });

            context.EmployeeShifts.AddRange(
                new DataModel.EmployeeShift { EmployeeId = 1, ShiftId = 1 },
                new DataModel.EmployeeShift { EmployeeId = 2, ShiftId = 1 },
                new DataModel.EmployeeShift { EmployeeId = 3, ShiftId = 1 },
                new DataModel.EmployeeShift { EmployeeId = 4, ShiftId = 1 },

                new DataModel.EmployeeShift { EmployeeId = 1, ShiftId = 2 },
                new DataModel.EmployeeShift { EmployeeId = 5, ShiftId = 2 },
                new DataModel.EmployeeShift { EmployeeId = 6, ShiftId = 2 },

                new DataModel.EmployeeShift { EmployeeId = 1, ShiftId = 4 },
                new DataModel.EmployeeShift { EmployeeId = 4, ShiftId = 4 },
                new DataModel.EmployeeShift { EmployeeId = 6, ShiftId = 4 },
                new DataModel.EmployeeShift { EmployeeId = 2, ShiftId = 4 },

                new DataModel.EmployeeShift { EmployeeId = 6, ShiftId = 5 },
                new DataModel.EmployeeShift { EmployeeId = 2, ShiftId = 5 },
                new DataModel.EmployeeShift { EmployeeId = 4, ShiftId = 5 },
                new DataModel.EmployeeShift { EmployeeId = 5, ShiftId = 5 },

                new DataModel.EmployeeShift { EmployeeId = 1, ShiftId = 6 },
                new DataModel.EmployeeShift { EmployeeId = 2, ShiftId = 6 },
                new DataModel.EmployeeShift { EmployeeId = 3, ShiftId = 6 },
                new DataModel.EmployeeShift { EmployeeId = 6, ShiftId = 6 },

                new DataModel.EmployeeShift { EmployeeId = 3, ShiftId = 7 },
                new DataModel.EmployeeShift { EmployeeId = 5, ShiftId = 7 },
                new DataModel.EmployeeShift { EmployeeId = 2, ShiftId = 7 },

                new DataModel.EmployeeShift { EmployeeId = 1, ShiftId = 8 },
                new DataModel.EmployeeShift { EmployeeId = 5, ShiftId = 8 },
                new DataModel.EmployeeShift { EmployeeId = 2, ShiftId = 8 },
                new DataModel.EmployeeShift { EmployeeId = 3, ShiftId = 8 });

            context.SaveChanges();
        }
    }
}
