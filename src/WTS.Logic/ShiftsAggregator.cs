﻿using System;
using System.Collections.Generic;
using System.Linq;
using WTS.Logic.LogicModels;

namespace WTS.Logic
{
    public class ShiftsAggregator : IShiftsAggregator
    {
        public double AggregateShiftsHours(IEnumerable<Shift> shifts, DateTime startTime, DateTime endTime)
        {
            if (!shifts.Any())
            {
                return 0;
            }

            return shifts
                .Select(s => TimeSpan.FromTicks(Math.Min(endTime.Ticks, s.EndTime.Ticks) - Math.Max(startTime.Ticks, s.StartTime.Ticks)))
                .Aggregate((t1, t2) => t1 + t2).TotalHours;
        }
    }
}