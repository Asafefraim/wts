﻿using System.Collections.Generic;

namespace WTS.Logic.LogicModels
{
    public class EmployeeShifts
    {
        public Employee Employee { get; set; }
        public IEnumerable<Shift> Shifts { get; set; }
    }
}