﻿using System;

namespace WTS.Logic.LogicModels
{
    public class Shift
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}