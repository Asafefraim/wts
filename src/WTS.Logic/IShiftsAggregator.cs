﻿using System;
using System.Collections.Generic;
using WTS.Logic.LogicModels;

namespace WTS.Logic
{
    public interface IShiftsAggregator
    {
        double AggregateShiftsHours(IEnumerable<Shift> shifts, DateTime startTime, DateTime endTime);
    }
}