﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WTS.Models;
using WTS.Data;
using AutoMapper;
using WTS.Logic;
using System;

namespace WTS.Controllers
{
    public class HomeController : Controller
    {
        private readonly IShiftsRepository _shiftsRepository;
        private readonly IMapper _mapper;
        private readonly IShiftsAggregator _aggregator;

        public HomeController(IShiftsRepository shiftsRepository, IMapper mapper, IShiftsAggregator aggregator)
        {
            _shiftsRepository = shiftsRepository;
            _mapper = mapper;
            _aggregator = aggregator;
        }

        public IActionResult Index()
        {
            var employees = _shiftsRepository.ListEmployees();
            var mapped = _mapper.Map<IEnumerable<Models.Employee>>(employees);
            return View(
                new QueryMonthlyHours
                {
                    Employees = mapped
                });
        }

        [HttpPost]
        public IActionResult GetMonthlyHours(QueryMonthlyHours request)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction(nameof(Error));
            }

            var monthSelection = request.SelectedMonth.Value;
            var startOfMonth = new DateTime(monthSelection.Year, monthSelection.Month, 1);
            var endOfMonth = startOfMonth.AddMonths(1).AddTicks(-1);
            var employeeShifts = _shiftsRepository.GetEmployeeShifts(request.EmployeeId.Value, startOfMonth, endOfMonth);

            var aggregatedHours = _aggregator.AggregateShiftsHours(employeeShifts.Shifts, startOfMonth, endOfMonth);

            return PartialView(new MonthlyHoursResults {
                FirstName = employeeShifts.Employee.FirstName,
                LastName = employeeShifts.Employee.LastName,
                ScheduledHours = aggregatedHours });
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
