﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WTS.Data;
using WTS.Logic;

namespace WTS
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddScoped<IShiftsRepository, ShiftsRepository>();
            services.AddScoped<IMapper, Mapper>(serviceProvider => new Mapper(new MapperConfiguration(CreateMaps)));
            services.AddScoped<IShiftsAggregator, ShiftsAggregator>();

            // TODO: Use SQL Server if needed?
            services.AddDbContext<DataContext>(
                options => options.UseInMemoryDatabase("ShiftsDb"));
        }

        private void CreateMaps(IMapperConfigurationExpression config)
        {
            config.CreateMap<Logic.LogicModels.Employee, Models.Employee>();

            config.CreateMap<Data.DataModel.Employee, Logic.LogicModels.Employee>();
            config.CreateMap<Data.DataModel.Shift, Logic.LogicModels.Shift>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
