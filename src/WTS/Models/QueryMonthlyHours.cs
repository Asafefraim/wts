﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WTS.Models
{
    public class QueryMonthlyHours
    {
        public IEnumerable<Employee> Employees { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please select an employee.")]
        public int? EmployeeId { get; set; }

        [Required(ErrorMessage = "Please specify a month.")]
        public DateTime? SelectedMonth { get; set; }
    }
}
