﻿Thanks for giving me the chance to complete this exercise.
While I'm thoroughly familiar with the tech stack I used here, I took it as a personal challenge to use the latest tech versions and utilise the new features that they offer.
This is why I chose to use .NET Core 2, ASP.NET Core 2 (MVC), Entity Framework Core 2, etc.

Here are some of the features that I found helpful -
ASP.NET Core 2 allows you to define client-side input validations using DataAnnotation attributes on the model classes, which is pretty cool!
Entity Framework Core 2 offers neat DI support so you can inject your DbContext class easily.
Entity Framework Core 2 also supports an in-memory database, which I used in this challenge to ensure it runs on any dev machine without any special setup.

I wrote the code top-down using TDD.

The challenge contains a certain level of ambiguity as to what the application's UI should look like, so I took the liberty of adding a form in.
Putting the usability discussion aside (I mean, who is the customer? What's the intended use?), I feel that this provides a better showcase of my skills.

You can find a deployed version of the application in the link below.
November 2016 is a good month to put in the form if you want a month with a lot of shifts.
http://wts20180418104442.azurewebsites.net

And the source code can be found in this bitbucket repository:
https://bitbucket.org/Asafefraim/wts/src